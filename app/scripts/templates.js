
var templates = {
  marker : {},
  baseMarkerParams : {},
  init : function() {
    this.baseMarkerParams = Object.assign({}, dials.player.args);
    this.baseMarkerParams.constructorFunctions = {};
    this.marker = new Component(
      Object.assign(this.baseMarkerParams, {
        x : 700,
        y : dials.text.baseParams.gameInfoHeight - 15,
        width : 15,
        height : 15,
      })
    );
    console.log(this.marker);
    console.log('templates initialized');
  },
};
