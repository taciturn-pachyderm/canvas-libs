var soundsBase = {
  buildManySounds : function(file, volume, qty) {
    let soundArray = [];
    while (soundArray.length < qty) {
      soundArray.push(new Sound(this.path + file, volume));
    };
    return soundArray;
  },
  playSound : function(type) {
    if (dials.game.sounds.value) {
      let sound = this.getSound(type);
      if (!sound) {
        return;
      }
      sound.play();
    };
  },
  getSound : function(type) {
    let target = this.tracks[type];
    if (!target) {
      return;
    };
    target.index = (target.index + 1) % target.pool.length;
    return target.pool[target.index];
  },
  stopAllSounds : function() {
    Object.keys(this.tracks).forEach(key => this.stopSound(this.tracks[key].pool));
  },
  stopSound : function(pool) {
    pool.forEach(sound => sound.stop());
  },
};
