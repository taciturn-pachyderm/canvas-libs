var hud = {
  elements : ['lives', 'score'],
  update : function() {
    metrics.livesMarker.update();
    this.elements.forEach(element =>
      this.updateElement(element)
    );
  },
  updateElement : function(element) {
    texts[element].text = metrics[element].player1;
    texts[element].update();
  },
};
