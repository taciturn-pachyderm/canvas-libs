function Component(args) {
  this.remove = false;
  this.speedX = 0;
  this.speedY = 0;
  this.x = args.x;
  this.y = args.y;
  this.directionY = args.directionY;
  this.width = args.width;
  this.height = args.height;
  this.defaultWidth = args.defaultWidth;
  this.defaultHeight = args.defaultHeight;
  this.hitPoints = args.extraArgs.hitPoints ? args.extraArgs.hitPoints : 0;
  if (args.background) {
    this.background = args.background;
  };
  this.shape = args.shape;
  this.color = args.color;
  if (args.fontSize) {
    this.fontSize = args.fontSize;
  };
  this.name = args.name;
  if (args.sprites) {
    this.getSpriteKey = args.getSpriteKey;
    this.sprites = supporting.clone(args.sprites);
    images.init(this.sprites);
  };
  if (args.extraArgs) {
    this.type = args.extraArgs.type;
    this.animationInterval = args.extraArgs.animationInterval;
    if (args.extraArgs.speed) {
      this.speedX = args.extraArgs.speed.x;
      this.speedY = args.extraArgs.speed.y;
    };
  };
  if (args.constructorFunctions) {
    Object.keys(args.constructorFunctions)
      .forEach(theFunction => args.constructorFunctions[theFunction](this));
  };
  this.update = function() {
    if (this.sound) {
      this.sound.play();
    };
    if (this.background) {
      this.background.update();
    };
    let ctx = game.gameArea.context;
    ctx.fillStyle = this.color;
    if (this.type == 'text') {
      this.makeText(ctx);
    } else if (this.shape == 'rectangle' || ['background', 'laser'].includes(this.type)) {
      this.makeARectangle(ctx);
    } else if (dials.components.imageTypes.includes(this.type)) {
      this.drawComponent(ctx, this);
    };
  };
  this.stop = function() {
    this.speedX = 0;
    this.speedY = 0;
  },
  this.makeText = function(ctx) {
    ctx.font = this.fontSize + " " + dials.text.font;
    ctx.fillText(this.text, this.x, this.y);
  };
  this.makeARectangle = function(ctx) {
    ctx.fillRect(this.x, this.y, this.width, this.height);
  };
  this.newPos = function() {
    this.x += this.speedX;
    this.y += this.speedY;
  };
  this.crashWith = function(otherObject) {
    let crash = true;
    if (this.getBottom() < otherObject.getTop() || this.getTop() > otherObject.getBottom() || this.getRight() < otherObject.getLeft() || this.getLeft() > otherObject.getRight()) {
      crash = false;
    };
    return crash;
  };
  this.crashWithXOnly = function(otherObject) {
    let crash = true;
    // delay collision slightly by allowing objects to overlap by 1 pixel
    if (this.getRight() < otherObject.getLeft() + 1 || this.getLeft() > otherObject.getRight() - 1) {
      crash = false;
    };
    return crash;
  };
  this.crashWithYOnly = function(otherObject) {
    let crash = true;
    if (this.getBottom() < otherObject.getTop() + 1 || this.getTop() > otherObject.getBottom() - 1) {
      crash = false;
    };
    return crash;
  };
  this.crashWithMiddle = function(otherObject) {
    let crash = false;
    if (this.crashWithMiddleX(otherObject) && this.crashWithYOnly(otherObject)) {
      crash = true;
    };
    return crash;
  };
  this.crashWithMiddleX = function(otherObject) {
    let thisMiddleX = this.getMiddleX();
    let otherMiddleX = otherObject.getMiddleX();
    return thisMiddleX < otherMiddleX + 5 && thisMiddleX > otherMiddleX - 5 && this.crashWithYOnly(otherObject);
  };
  this.crashWithMiddleY = function(otherObject) {
    let thisMiddleY = this.getMiddleY();
    let otherMiddleY = otherObject.getMiddleY();
    return thisMiddleY < otherMiddleY + 5 && thisMiddleY > otherMiddleY - 5;
  }
  this.getMiddleX = function() {
    return this.x + this.width / 2;
  };
  this.getMiddleY = function() {
    return this.y + this.height / 2;
  };
  this.getTop = function() {
    return this.y;
  };
  this.getBottom = function() {
    return this.y + this.height;
  };
  this.getLeft = function() {
    return this.x;
  };
  this.getRight = function() {
    return this.x + this.width;
  };
  this.drawComponent = function(ctx, obj) {
    if (!obj.sprites) {
      throw 'no sprites found';
    };
    let sprite = images.get(obj);
    if (!sprite) {
      throw 'no sprite match';
    };
    ctx.drawImage(sprite, obj.x, obj.y, obj.width, obj.height);
  };
};
