var supporting = {
  intervalDivisor : 5,
  fieldToCompare : '',
  everyinterval : function(frameNo, interval) {
    let divisor = interval ? interval : 1;
    if ((frameNo / divisor) % 1 == 0) {
      return true;
    };
    return false;
  },
  getRandom : function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  },
  roll : function(sides) {
    let theRoll = Math.floor(Math.random() * sides) + 1;
    return {value : theRoll, crit : theRoll == sides};
  },
  getTime : function(frameNo) {
    return Math.ceil(frameNo / ( 1000 / this.intervalDivisor ));
  },
  getClosest : function(array, target) {
    let closest = array.reduce(function(prev, curr) {
      return (Math.abs(curr - target) < Math.abs(prev - target) ? curr : prev);
    });
    return closest;
  },
  getFirstTruthy : function(entries) {
    return Object.keys(entries).find(entry => entries[entry]);
  },
  isMobile : function() {
    return ( location.search.indexOf( 'ignorebrowser=true' ) < 0 && /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test( navigator.userAgent ) );
  },
  isChrome : function() {
    return navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
  },
  // TODO get rid of this; centipede handles delays well without a wait (by using frames and the interval); change robotron to do the same
  wait : function(ms) {
    var d = new Date();
    var d2 = null;
    do {
      d2 = new Date();
    } while(d2 - d < ms);
  },
  align : function(string) {
    let starting = 15;
    let result = '';
    for (let i = 0; i < starting - string.length; i++) {
      result += ' ';
    };
    return string + result;
  },
  compare : function(a, b) {
    let field = supporting.fieldToCompare;
    if (a[field] < b[field]) {
      return 1;
    } else if (a[field] > b[field]) {
      return -1;
    } else {
      return 0;
    };
  },
  clone : function(obj) {
    // deep clone; does not work with functions
    return JSON.parse(JSON.stringify(obj));
  },
  isWholeArrayPresent : function(needles, haystack) {
    for (var i = 0; i < needles.length; i++) {
      if (!haystack[needles[i]]) {
        return false;
      }
    };
    return true;
  },
  addJSMemoryMonitor : function() {
    var script = document.createElement('script');
    script.src = 'https://rawgit.com/paulirish/memory-stats.js/master/bookmarklet.js';
    document.head.appendChild(script);
  },
  getRandomDirection : function() {
    let random = Math.floor(Math.random() * 199) - 99;
    if (!random) return this.getRandomDirection();
    return random;
  },
  getIndex : function(list, current, shift) {
    let n = list.length;
    return (((current + shift) % n) + n) % n;
  },
  applyOverrides : function(obj) {
    Object.keys(obj.functionOverrides).forEach(element => {
      obj[element] = obj.functionOverrides[element];
    });
  },
};
