var collisionsBase = {
  checkLaser : function(player, targets, lasers) {
    lasers[player].forEach(laser =>
      targets.forEach(target => {
        if (!laser.remove && laser.crashWith(target)) {
          this.processImpact(target);
          laser.remove = true;
        };
      })
    );
  },
  processImpact : function(target) {
    this.damageTarget(target);
    sounds.playSound('impact');
  },
  damageTarget : function(target) {
    target.hitPoints--;
    if (target.hitPoints <= 0) {
      this.processKill(target);
      return;
    };
    if (target.sizeMod) {
      this.changeSize(target);
    };
  },
  processKill : function(target) {
    metrics.addNewFloatingPoint(target.getMiddleX(), target.getMiddleY(), target.pointValue, "gain");
    metrics.manageScore(target.pointValue);
    this.handleSpecialKills(target);
  },
  handleSpecialKills : function(target) {
    // override in collisions object to add special kill logic
    return;
  },
  changeSize : function(target) {
    let widthBefore = target.width;
    let heightBefore = target.height;
    target.width = target.defaultWidth * target.hitPoints;
    target.height = target.defaultHeight * target.hitPoints;
    target.x += (widthBefore - target.width) / 2;
    target.y += (heightBefore - target.height) / 2;
  },
  getPlayerEnemies : function() {
    let enemies = [];
    return enemies;
  },
  getLaserTargets : function() {
    let targets = [];
    return targets;
  },
  killPlayer : function() {
    players.died = true;
    metrics.lives.player1 -= 1;
    if (metrics.lives.player1 <= 0) {
      game.gameOver = true;
      return;
    };
  },
  check : function() {
    throw 'implement me: check()';
  },
  checkPlayerVsEnemies : function(player, targets) {
    throw 'implement me: checkPlayerVsEnemies()';
  },
  removeDestroyedTargets : function(targets) {
    throw 'implement me: removeDestroyedTargets()';
  },
};
