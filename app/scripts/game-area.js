function GameArea() {
  this.init = function(props) {
    this.canvas = document.createElement("canvas"),
    this.canvas.width = props.canvas.width;
    this.canvas.height = props.canvas.height;
    this.gridSquareSideLength = props.general.gridSquareSideLength;
    this.gridStart = props.general.gridSquareSideLength * 2;
    this.xVertices = [],
    this.yVertices = [],
    this.setGridVertices();
    console.log('gameArea initialized');
  };
  this.start = function() {
    console.log('starting game-area');
    this.loadCanvas();
    this.loadFrame();
    this.loadFont();
    this.loadBackground();
    this.drawGridVertices();
  };
  this.loadCanvas = function() {
    this.context = this.canvas.getContext("2d");
    document.getElementById("canvas-wrapper").appendChild(this.canvas);
  };
  this.loadFrame = function() {
    this.frameNo = 0;
    this.interval = setInterval(main.updateGameState, supporting.intervalDivisor);
  };
  this.loadFont = function() {
    let theLoadedFont = new FontFace('press-start', 'url(./app/static/css/fonts/prstartk.ttf)');
    theLoadedFont.load().then((font) => {
      document.fonts.add(font);
      console.log('Font added', font);
    });
  };
  this.loadBackground = function() {
    let backgroundId = 'canvas-background';
    if (!document.getElementById(backgroundId)) {
      let canvasBackground = new Image();
      console.log('setting background gif');
      canvasBackground.src = dials.dom.gifLocation;
      canvasBackground.id = backgroundId;
      document.getElementById("canvas-wrapper").appendChild(canvasBackground);
    };
  };
  this.removeBackground = function() {
    let background = document.getElementById('canvas-background');
    if (!background) {
      return;
    };
    background.parentNode.removeChild(background);
  };
  this.setGridVertices = function() {
    this.xVertices = this.getXVertices();
    this.yVertices = this.getYVertices();
  };
  this.getXVertices = function() {
    let x = 0;
    let vertices = [];
    while (x < this.canvas.width) {
      vertices.push(Math.ceil(x));
      x += this.gridSquareSideLength;
    };
    return vertices;
  };
  this.getYVertices = function() {
    let y = this.gridStart;
    let vertices = [];
    while (y < this.canvas.height) {
      vertices.push(Math.ceil(y));
      y += this.gridSquareSideLength;
    };
    return vertices;
  };
  this.drawGridVertices = function() {
    // NOTE Do not use this in the main game loop unless measuring placement
    // it is super slow
    this.xVertices.forEach((x, i) => {
      this.yVertices.forEach((y, j) => {
        this.drawLine(x, y, this.gridSquareSideLength, 0);
        this.drawLine(x, y, 0, this.gridSquareSideLength);
      });
    });
  };
  this.drawLine = function(x, y, xmod, ymod) {
    this.context.beginPath();
    this.context.moveTo(x, y);
    this.context.lineTo(x + xmod, y + ymod);
    this.context.stroke();
  };
  this.clear = function() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  };
  this.stop = function() {
    clearInterval(this.interval);
  };
};
