var menus = {
  currentSelection : undefined,
  init : function() {
    Object.assign(this, menusProps);
    this.screens.initials = initials;
    this.screens.main = mainMenu;
    this.selectionMarker = Object.assign({}, templates.marker);
    console.log('menus initialized');
  },
  areActive : function() {
    supporting.getFirstTruthy(this.show);
  },
  reset : function() {
    game.gameOver = false;
    this.init();
    this.display(metrics.lastScore ? 'initials' : 'main');
  },
  display : function(menu) {
    menus.disableMenus();
    menus.show[menu] = true;
  },
  disableMenus : function() {
    if (game.gameArea.frameNo > 0) {
      main.prepTheCanvas();
    };
    this.timeSinceSelection = 0;
    Object.keys(this.show).forEach(menu => this.show[menu] = false);
  },
  processMenus : function() {
    this.checkForCheats();
    if (this.show.initials) {
      this.manageInitials();
    } else if (this.show.main) {
      this.screens.main.refresh();
    } else if (this.show.playerActivate) {
      this.setGamepadText();
    };
    let screen = this.getCurrentScreen();
    this.drawMenu(screen);
  },
  checkForCheats : function() {
    if (this.show.initials && game.cheatsAreActive()) {
      this.show.initials = false;
      this.show.main = true;
    };
  },
  manageInitials : function() {
    if (!this.currentSelection.entry || this.timeSinceMenuMove < this.minTimeToMove) {
      return;
    };
    let texts = this.screens.initials.text;
    this.setTexts(texts);
    this.setPositions(texts);
  },
  setTexts : function(texts) {
    let initials = this.screens.initials;
    initials.text.currentScore.text = 'your score ' + metrics.lastScore;
    initials.letterDisplays.forEach(entry =>
      texts[entry].text = 'DONE'
    );
    if (this.currentSelection.entry.options) {
      this.shiftListOrder(this.currentSelection.entry, 'options');
      let options = this.currentSelection.entry.options;
      this.setShadowerTexts(texts, options, initials.activeIndexShifts);
      initials.entries[this.currentSelection.name].text = options[this.getIndex(options, initials.activeIndexShifts.current)];
    };
  },
  setShadowerTexts : function(texts, options, indices) {
    texts.previouser.text = options[this.getIndex(options, indices.previouser)];
    texts.previous.text = options[this.getIndex(options, indices.previous)];
    texts.next.text = options[this.getIndex(options, indices.next)];
    texts.nexter.text = options[this.getIndex(options, indices.nexter)];
  },
  getIndex : function(options, shift) {
    let current = this.currentSelection.entry.activeIndex;
    return supporting.getIndex(options, current, shift);
  },
  setPositions : function(texts) {
    let xPosition = this.currentSelection.entry.component.x;
    let yPosition = this.currentSelection.entry.component.y;
    this.screens.initials.letterDisplays.forEach(entry => {
      texts[entry].xOverride = xPosition;
      texts[entry].yOverride = yPosition;
    });
  },
  selectNextInitial : function() {
    let initials = this.screens.initials;
    let current = initials.activeIndex;
    let direction = controls.checkMenuDirection();
    let shift = 0;
    if (['left'].includes(direction)) {
      shift = -1;
      this.timeSinceMenuMove = 0;
    } else if (['right'].includes(direction)) {
      shift = 1;
      this.timeSinceMenuMove = 0;
    };
    initials.activeIndex = supporting.getIndex(Object.keys(initials.entries), current, shift);
  },
  getCurrentScreen : function() {
    let activeScreen = supporting.getFirstTruthy(this.show);
    let screen = {};
    if (activeScreen) {
      screen = this.screens[activeScreen];
    };
    return screen;
  },
  drawMenu : function(screen) {
    if (!this.hasElements(screen)) {
      return;
    };
    main.prepTheCanvas();
    if (screen.update) {
      screen.update();
    };
    this.drawTexts(menus.commonTexts);
    this.drawEntries(screen.entries);
    if (screen.order) {
      this.setMenuOrder(screen);
    };
    this.checkForSelection();
    if (!screen.ignoreMarker) {
      this.drawSelectionMarker();
    };
    if (screen.text) {
      this.drawTexts(screen.text);
    };
  },
  hasElements : function(object) {
    return Object.keys(object).length > 0;
  },
  setMenuOrder : function(screen) {
    this.timeSinceMenuMove += 1;
    if (this.timeSinceMenuMove > this.minTimeToMove) {
      this.shiftListOrder(screen, 'order');
    };
    this.currentSelection.name = screen.order[screen.activeIndex];
  },
  shiftListOrder : function(screen, list) {
    let current = screen.activeIndex;
    let direction = controls.checkMenuDirection();
    if (!direction) {
      return;
    };
    let shift = 0;
    if (['up'].includes(direction)) {
      this.timeSinceMenuMove = 0;
      shift = -1;
    } else if (['down'].includes(direction)) {
      this.timeSinceMenuMove = 0;
      shift = 1;
    };
    screen.activeIndex = supporting.getIndex(screen[list], current, shift);
  },
  drawEntries : function(entries) {
    Object.keys(entries).forEach((entry, index) => {
      let menuElement = entries[entry];
      if (!menuElement.component) {
        menuElement.component = this.buildDefaultComponent();
      };
      menuElement.component.x = menuDefaults.entries.x + (menuElement.xAdjust ? menuElement.xAdjust : 0);
      menuElement.component.y = menuDefaults.entries.y + (menuElement.yAdjust ? menuElement.yAdjust : 0) + menuDefaults.yDivider * index;
      menuElement.component.text = menuElement.text;
      if (menuElement.fontSize) {
        menuElement.component.fontSize = menuElement.fontSize;
      };
      if (this.currentSelection.name == entry && !menuElement.noSelection) {
        this.currentSelection.entry = menuElement;
        this.currentSelection.color = this.selectedEntryColor;
      };
      menuElement.component.update();
    });
  },
  drawSelectionMarker : function() {
    if (!this.currentSelection.entry) {
      return;
    };
    this.selectionMarker.x = this.currentSelection.entry.component.x - templates.baseMarkerParams.width * 2;
    this.selectionMarker.y = this.currentSelection.entry.component.y - templates.baseMarkerParams.height;
    this.selectionMarker.update();
  },
  drawTexts : function(texts) {
    Object.keys(texts).forEach((key, index) => {
      let entry = texts[key];
      if (!entry.component) {
        entry.component = this.buildDefaultComponent();
      };
      entry.component.x = (entry.xOverride ? entry.xOverride : menuDefaults.text.x) + (entry.xAdjust ? entry.xAdjust : 0);
      entry.component.y = (entry.yOverride ? entry.yOverride : menuDefaults.text.y) + (entry.yAdjust ? entry.yAdjust : 0) + menuDefaults.yDivider * index;
      entry.component.text = entry.text ? entry.text : entry.base;
      if (entry.fontSize) {
        entry.component.fontSize = entry.fontSize;
      };
      if (entry.color) {
        entry.component.color = entry.color;
      };
      entry.component.update();
    });
  },
  buildDefaultComponent : function() {
    return new Component(dials.text.baseParams);
  },
  checkForSubmit : function() {
    this.timeSinceSelection += 1;
    if (
      this.timeSinceSelection > this.minTimeToSelect
        &&
      controls.submitIsPressed()
        &&
      this.currentSelection.entry.submit
    ) {
    };
  },
  checkForSelection : function() {
    this.timeSinceSelection += 1;
    if (this.timeSinceSelection > this.minTimeToSelect) {
      if (controls.menuSelect()) {
        this.currentSelection.entry.action();
      } else if (controls.submitIsPressed() && this.currentSelection.entry.submit) {
        this.currentSelection.entry.submit();
      };
    };
  },
  setGamepadText : function() {
    let gamepadsEnabled = controls.gamepad.enabledGamepadIndices.size;
    let playerActivateEntries = this.screens.playerActivate.text;
    playerActivateEntries.gamepadCount.text = "Active gamepads: " + gamepadsEnabled;
    if (gamepadsEnabled) {
      game.activePlayers = gamepadsEnabled;
      let key = 'player' + gamepadsEnabled + 'Check';
      entry = playerActivateEntries[key];
      entry.text = entry.base + 'ACTIVE';
    };
  },
};
