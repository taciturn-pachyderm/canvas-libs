var appPath = 'app/scripts/';
var testPath = 'test/spec/';

var appScripts = [
  'collisions-base.js',
  'component.js',
  'controls/gamepad.js',
  'controls.js',
  'controls/keyboard.js',
  'dom.js',
  'game-area.js',
  'game-base.js',
  'game-objects-base.js',
  'hud.js',
  'images.js',
  'init/init.js',
  'leaderboard.js',
  'main-base.js',
  'menus/initials.js',
  'menus.js',
  'menus/main-menu.js',
  'metrics.js',
  'players-base.js',
  'sound.js',
  'sounds-base.js',
  'supporting.js',
  'templates.js',
  'texts.js',
];

var testScripts = [
  'sound-spec-centipede.js',
  'main-spec.js',
  'supporting-spec.js',
  'hud-spec.js',
  'menus-spec.js',
  'dom-spec.js',
  'controls-spec.js',
  'collisions-spec.js',
  'jasmine-libs/jasmine.min.js',
  'jasmine-libs/boot.min.js',
  'jasmine-libs/jasmine-html.js',
  'game-spec.js',
  'sound-spec.js',
  'texts-spec.js',
  'players-spec.js',
  'properties/dials.js',
  'properties/menus-props.js',
  'game-area-spec.js',
  'component-spec.js',
  'controls/gamepad-spec.js',
  'controls/keyboard-spec.js',
]

var runners = [
  'init/runner.js',
];

function loadScript (path, file) {
  var script = document.createElement("script");
  script.src = path + file;
  document.body.appendChild(script);
};

appScripts.forEach(file => loadScript(appPath, file));
testScripts.forEach(file => loadScript(testPath, file));
setTimeout(function() {
  runners.forEach(file => loadScript(appPath, file));
}, 500);

console.log(document.body);
