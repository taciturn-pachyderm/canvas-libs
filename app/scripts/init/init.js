var libsInit = {
  run : function() {
    console.log('start canvas-libs init');

    initials.init();
    mainMenu.init();
    menus.init();

    gamepad.init();
    controls.init();

    templates.init();

    gameBase.init();

    console.log("canvas-libs initialized");
  },
};
