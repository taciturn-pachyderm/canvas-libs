var controls = {
  init : function() {
    this.gamepad = gamepad;
    this.keyboard = keyboard;
    this.keyboard.init();
    console.log('controls initialized');
  },


  // API Access: get new position, accessed by players.js
  getPositionModifiers : function(boundaries, baseSpeed, player) {
    this.gamepad.boundaries = boundaries;
    let mods = this.buildSpeedMods(this.gamepad.getObjectSpeed(baseSpeed, player));
    return mods[this.getActiveDirection(player)];
  },
  buildSpeedMods : function(objSpeed) {
    return {
      upRight: {x : objSpeed.x, y : -objSpeed.y},
      upLeft: {x : -objSpeed.x, y : -objSpeed.y},
      downRight: {x : objSpeed.x, y : objSpeed.y},
      downLeft: {x : -objSpeed.x, y : objSpeed.y},
      right: {x : objSpeed.x},
      down: {y : objSpeed.y},
      left: {x : -objSpeed.x},
      up: {y : -objSpeed.y},
    };
  },
  getActiveDirection : function(player) {
    let directionResults = this.getValidDirections(player);
    return supporting.getFirstTruthy(directionResults);
  },
  getValidDirections : function(player) {
    let directionResults = {};
    Object.keys(this.keyboard.movementCodes).forEach(directionCode =>
      directionResults[directionCode] =
        this.checkDirection(directionCode, player) && player.eligibleDirections[directionCode]
    );
    return directionResults;
  },
  checkDirection : function(directionCode, player) {
    let result =
      supporting.isWholeArrayPresent(this.keyboard.movementCodes[directionCode], this.keyboard.keysDown)
        ||
      this.gamepad.validateDirection(directionCode, 'leftStick', this.gamepad.activeGamepads[player.name]);
    return result;
  },


  // API access: get paused button state, accessed by main.js
  pausedIsPressed : function() {
    return this.gamepad.pausePressed() || this.keyboard.flowControlButtonPressed();
  },

  submitIsPressed : function() {
    return this.pausedIsPressed();
  },
  
  // API access: get firing state, accessed by lasers.js
  isFiring : function(player) {
    return this.gamepad.isFiring(player) || this.keyboard.isFiring();
  },


  // API access: get menu select, accessed by menus.js
  menuSelect : function() {
    return this.gamepad.menuSelect() || this.keyboard.menuSelectButtonPressed();
  },


  // API access: get menu change, accessed by menus.js
  checkMenuDirection : function() {
    let direction = "";
    ['up', 'down', 'left', 'right'].forEach(dir => {
      direction = direction == "" ? this.detectMenuChange(dir, direction) : direction;
    });
    return direction;
  },
  detectMenuChange : function(checkDir, currentDir) {
    return this.gamepad.detectMenuChange(checkDir, currentDir) || this.keyboard.detectMenuChange(checkDir, currentDir);
  },
};
