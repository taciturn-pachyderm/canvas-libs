
var dom = {
  links : {
    devblog : {text : 'dev blog.', url : ''},
    source : {text : 'gitlab.', url : ''},
    otherGames : {text : 'other games.', url : 'https://games.matthewodle.com'},
  },
  warnings : {
    mobile: "",
    notChrome: "",
  },
  mobileWarning : "",
  init : function() {
    this.links.devblog.url = dials.dom.blogUrl;
    this.links.source.url = dials.dom.sourceUrl;
    this.warnings.mobile = "Mobile is not supported.<br><br>" +
      "The use of a keyboard is required.<br><br>" +
      "Sorry!<br><br>" +
      "To show how bad we feel, here's a gif so you can see what you're missing (that's not rude at all, we promise!)<br><br>" +
      "<img src='" + dials.dom.gifLocation + "' style='width: 100%;'></img>" +
      "<br><br><br><br><br><br><br><br>."
    this.warnings.notChrome = "This game only supports <a target=_blank href='https://www.google.com/chrome/'>Google Chrome</a>.<br><br>" +
      "Sorry!<br><br>"
    this.warnings.notChrome += dials.dom.gifLocation ? "To show how bad we feel, here's a gif so you can see what you're missing (that's not rude at all, we promise!)<br><br><img src='" + dials.dom.gifLocation + "'></img><br><br><br><br><br><br><br><br>." : ""
    this.addElement(this.getLinksElement());
    if (supporting.isMobile() || !supporting.isChrome()) {
      let messageHTML = supporting.isMobile() ? this.warnings.mobile : this.warnings.notChrome;
      let elem = document.getElementById("canvas-wrapper");
      elem.parentElement.removeChild(elem);
      this.addElement(this.getMessageElement(messageHTML));
      return;
    };
    console.log("dom initialized");
  },
  addElement : function(element) {
    document.body.insertBefore(element, document.body.childNodes[0]);
  },
  getLinksElement : function() {
    let element = document.createElement('div');
    element.className = 'linkButtonWrapper';
    Object.keys(this.links).forEach( link => {
      let aLink = document.createElement('div');
      aLink.className = 'linkButton';
      aLink.onclick = function() { window.open(dom.links[link].url) };
      aLink.innerHTML = this.links[link].text;
      element.appendChild(aLink);
    });
    return element;
  },
  getMessageElement : function(warning) {
    let element = document.createElement('div');
    element.innerHTML = warning;
    return element;
  },
};
