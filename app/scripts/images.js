var images = {
  init : function(sprites) {
    Object.keys(sprites).forEach(key => {
      sprites[key].files.forEach(entry => {
        sprites[key].images.push(this.getSprite(entry));
      })
    });
  },
  getSprite : function(name) {
    let sprite = new Image();
    sprite.onerror = function() { sprite.src = dials.brokenPath };
    sprite.src = dials.mediaPath + name;
    return sprite;
  },
  get : function(obj) {
    let key = obj.getSpriteKey(obj);
    this.cycleImageIndex(obj, key);
    let index = obj.sprites[key].activeIndex;
    return obj.sprites[key].images[index ? index : 0];
  },
  cycleImageIndex : function(obj, key) {
    if (!obj.animationInterval) {
      return;
    };
    if (supporting.everyinterval(game.gameArea.frameNo, obj.animationInterval)) {
      let target = obj.sprites[key];
      target.activeIndex = (target.activeIndex + 1) % target.images.length;
    };
  },
};
