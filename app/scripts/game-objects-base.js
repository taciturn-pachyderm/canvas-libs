var gameObjectsBase = {
  manage : function() {
    throw 'implement me: manage()';
  },
  spawn : function() {
    throw 'implement me: spawn()';
  },
  add : function() {
    throw 'implement me: add()';
  },
  generate : function() {
    throw 'implement me: generate()';
  },
  make : function() {
    throw 'implement me: make()';
  },
  update : function() {
    throw 'implement me: update()';
  },
  clear : function() {
    throw 'implement me: clear()';
  },
  clearOutsideCanvas : function() {
    throw 'implement me: clearOutsideCanvas()';
  },
};
