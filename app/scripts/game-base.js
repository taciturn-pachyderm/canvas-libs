var gameBase = {
  paused : true,
  running : false,
  gameOver : false,
  timeSinceGameOver : 0,
  delayed : 0,
  delayEndTime : 300,
  keysDown : {},
  activeCheats : {},
  numberOfPlayers : 1,
  activePlayers : 1,
  frameNo : 0,
  init : function() {
    this.gameArea = new GameArea();
    console.log('gameBase initialized');
  },
  start : function() {
    console.log('starting game');
    if (supporting.isMobile() || !supporting.isChrome()) {
      this.gameArea.stop();
      return;
    };
    menus.reset();
    this.paused = true;
    this.gameArea.start(); 
  },
  run : function() {
    this.paused = false;
    this.gameArea.frameNo = 0;
    this.running = true;
  },
  levelIsOver : function() {
    return this.gameLevelCheck();
  },
  startNextFrame : function() {
    this.gameArea.clear();
    this.gameArea.frameNo += 1;
  },
  getFrameNo : function() {
    return this.gameArea.frameNo;
  },
  cheatsAreActive : function() {
    return supporting.getFirstTruthy(game.activeCheats);
  },
  manageLevel : function() {
    console.log('managing level');
    this.gameResets.level();
    metrics.currentLevel += 1;
  },
  setDiedText : function() {
    texts.diedText.text = "You died.";
    texts.diedText.update();
  },
  managePause : function() {
    texts.pausedMessage.text = "Paused";
    texts.pausedMessage.update();
    sounds.stopAllSounds();
  },
  manageDeath : function() {
    this.gameResets.death();
    this.resetMoreThings();
    texts.diedText.text = "";
    players.died = false;
  },
  manageGameOver : function() {
    if (this.gameOver) {
      this.timeSinceGameOver += 1;
      sounds.stopAllSounds();
      this.showGameOver();
      if (this.timeSinceGameOver > dials.game.gameOverDelay) {
        this.resetTheWholeTamale();
      };
    };
  },
  showGameOver : function() {
    texts.gameOver.text = "Game Over";
    texts.gameOver.update();
  },
  resetMoreThings : function() {
    gameObjects.clear();
    players.reset();
  },
  resetTheWholeTamale : function() {
    this.gameOver = false;
    this.timeSinceGameOver = 0;
    metrics.lastScore = metrics.score.player1;
    menus.reset();
    this.gameResets.everything();
  },
  gameLevelCheck : function() {
    throw 'implement me: gameLevelCheck()';
  },
};
