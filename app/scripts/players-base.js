var playerConstants = {
  watchPositions : {
    'up' : ['belowTop'],
    'right' : ['insideRight'],
    'down' : ['aboveBottom'],
    'left' : ['insideLeft'],
    'upRight' : ['belowTop', 'insideRight'],
    'downRight' : ['aboveBottom', 'insideRight'],
    'downLeft' : ['aboveBottom', 'insideLeft'],
    'upLeft' : ['belowTop', 'insideLeft'],
  },
  eligibleDirections : {
    'up' : true,
    'right' : true,
    'down' : true,
    'left' : true,
    'upRight' : true,
    'downRight' : true,
    'downLeft' : true,
    'upLeft' : true,
  },
};

var playersBase = {
  players : {},
  activeDirection : undefined,
  boundaries : {},
  died : false,
  setBoundaries : function(player) {
    // abstract
    throw ('define abstract function setBoundaries in player implementation');
  },
  buildPlayers : function(numberOfPlayers, args) {
    while (Object.keys(this.players).length < numberOfPlayers) {
      let player = new Component(args);
      player.name = 'player' + (Object.keys(this.players).length + 1);
      player.eligibleDirections = supporting.clone(playerConstants.eligibleDirections);
      this.players[player.name] = player;
    };
  },
  manage : function() {
    Object.keys(this.players).forEach(key => {
      let player = this.players[key];
      this.move(player);
      this.update(player);
    });
  },
  update : function(player) {
    player.update();
  },
  reset : function() {
    console.log('resetting players');
    Object.keys(this.players).forEach(key => {
      let player = this.players[key];
      player.x = dials.player.startX[0];
      player.y = dials.player.startY || player.y;
    });
  },
  move : function(player) {
    this.stop(player);
    this.setBoundaries(player);
    this.determineEligibleDirections(player);
    this.moveTheThing(player);
  },
  stop : function(player) {
    player.speedX = 0;
    player.speedY = 0;
  },
  determineEligibleDirections : function(player) {
    player.eligibleDirections = supporting.clone(playerConstants.eligibleDirections);
    Object.keys(playerConstants.watchPositions).forEach(direction => {
      playerConstants.watchPositions[direction].forEach(playerPosition =>
        player.eligibleDirections[direction] = this.boundaries[playerPosition] && player.eligibleDirections[direction]
      );
    });
  },
  moveTheThing : function(player) {
    let speed = this.determineSpeed(player);
    if (!speed) {
      return;
    };
    this.updatePosition(player, speed);
    if (this.collidedWithBarrier(player)) {
      this.revertPosition(player, speed);
    };
  },
  determineSpeed : function(player) {
    return controls.getPositionModifiers(this.boundaries, dials.player.speed.value, player)
  },
  updatePosition : function(player, modifier) {
    player.speedX = modifier.x ? modifier.x : player.speedX;
    player.speedY = modifier.y ? modifier.y : player.speedY;
    player.newPos();
  },
  collidedWithBarrier : function(player) {
    throw 'implement me: collidedWithBarrier()';
  },
  revertPosition : function(player, modifier) {
    player.speedX = -1 * (modifier.x ? modifier.x : player.speedX);
    player.speedY = -1 * (modifier.y ? modifier.y : player.speedY);
    player.newPos();
  },
};
