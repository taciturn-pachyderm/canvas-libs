var keyboard = {
  keysDown : {},
  fireKeyCodes : [16, 37, 38, 39, 40, 'LMB'],
  flowControlKeyCodes : [13, 32],
  menuSelectKeyCodes : [13, 37, 39, 65, 68],
  fireCombos : {
    upRight : { name: 'upRight', keys : [38, 39], speed : { x : 0.79, y : -0.79} },
    downRight : { name: 'downRight', keys : [40, 39], speed : { x : 0.79, y : 0.79} },
    downLeft : { name: 'upLeft', keys : [40, 37], speed : { x : -0.79, y : 0.79} },
    upLeft : { name: 'upLeft', keys : [38, 37], speed : { x : -0.79, y : -0.79} },
    up : { name: 'up', keys : [38], speed : { x : 0, y : -1} },
    right : { name: 'right', keys : [39], speed : { x : 1, y : 0} },
    down : { name: 'down', keys : [40], speed : { x : 0, y : 1} },
    left : { name: 'left', keys : [37], speed : { x : -1, y : 0} },
  },
  movementCodes : {
    upRight : [68, 87],
    downRight : [68, 83],
    downLeft : [83, 65],
    upLeft : [65, 87],
    up : [87],
    right : [68],
    down : [83],
    left : [65],
  },
  menuCodes : {
    up : [87, 38],
    down : [83, 40],
    left : [37],
    right : [39],
  },
  init : function() {
    this.addEventListeners();
    this.setDefaults();
  },
  addEventListeners : function() {
    window.addEventListener('mousedown', function (e) {
      controls.keyboard.keysDown['LMB'] = (e.type === "mousedown" && event.which === 1);
    });
    window.addEventListener('mouseup', function (e) {
      controls.keyboard.keysDown['LMB'] = (e.type === "mousedown" && event.which === 1);
    });
    window.addEventListener('keydown', function (e) {
      controls.keyboard.keysDown[e.keyCode] = (e.type == "keydown");
    });
    window.addEventListener('keyup', function (e) {
      controls.keyboard.keysDown[e.keyCode] = (e.type == "keydown");
    });
  },
  setDefaults : function() {
    // controlsProps are provided by the implementing project and must be
    // added to that project's app/scripts/properties/controls-props.js
    if (!this.hasOwnProperty('controlsProps')) {
      return;
    }
    Object.keys(controlsProps.keyboard).forEach(prop =>
      this[prop] = controlsProps.keyboard[prop])
  },
  flowControlButtonPressed : function() {
    return this.getPressedKeys().some(key => this.flowControlKeyCodes.indexOf(parseInt(key)) >= 0);
  },
  menuSelectButtonPressed : function() {
    return this.getPressedKeys().some(key => this.menuSelectKeyCodes.indexOf(parseInt(key)) >= 0);
  },
  getPressedKeys : function() {
    return Object.keys(this.keysDown).filter(entry => this.keysDown[entry]);
  },
  isFiring : function() {
    return this.fireKeyCodes.find(key => this.keysDown[key]);
  },
  getFiringDirection : function() {
    firingDirectionResults = {}
    Object.keys(this.fireCombos).forEach(directionCode =>
      firingDirectionResults[directionCode] =
        supporting.isWholeArrayPresent(this.fireCombos[directionCode].keys, this.keysDown)
    );
    result = supporting.getFirstTruthy(firingDirectionResults);
    return this.fireCombos[result];
  },
  detectMenuChange : function(checkDir, currentDir) {
    let keysPushed = this.getPressedKeys();
    keysPushed.forEach(key =>
      currentDir =
        currentDir == ""
          &&
        this.menuCodes[checkDir].includes(parseInt(key))
          ? checkDir : currentDir
    );
    return currentDir;
  },
};
