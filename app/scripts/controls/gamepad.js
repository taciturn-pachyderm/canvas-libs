var gamepadTemplates = {
  axes : {
    leftStick : {
      indices : {x : 0, y : 1},
      values : {x : 0, y : 0},
      requiredArrayLength: 2,
    },
    rightStick : {
      indices : {x : 2, y : 3},
      values : {x : 0, y : 0},
      requiredArrayLength: 4,
    },
    dPad : {
      indices : {x : 4, y : 5},
      values : {x : 0, y : 0},
      requiredArrayLength: 6,
    },
  },
};

var gamepadConstants = {
  menuEnterIndices : [2],
  fireButtonIndices : [0, 1, 2, 3, 4, 5, 6, 7],
  pausedButtonIndex : 9,
  stickValueThreshold : 0.25,
  axesIndexLimit : 1,
  coordinateDirections : {
    upRight : {x : 1, y : -1},
    downRight : {x : 1, y : 1},
    downLeft : {x : -1, y : 1},
    upLeft : {x : -1, y : -1},
    up : {x : 0, y : -1},
    right : {x : 1, y : 0},
    down : {x : 0, y : 1},
    left : {x : -1, y : 0},
  },
}

var gamepad = {
  boundaries : undefined,
  gamepads : undefined,
  enabledGamepadIndices : new Set([]),
  activeGamepads : {
    player1 : {
      index : -1,
      axes : {},
      buttons : [],
    },
    player2 : {
      index : -1,
      axes : {},
      buttons : [],
    },
  },
  init : function() {
    this.activeGamepads.player1.axes = supporting.clone(gamepadTemplates.axes);
    this.activeGamepads.player2.axes = supporting.clone(gamepadTemplates.axes);
    console.log('gamepad initialized;', this);
  },


  // entry point
  checkState : function(numberOfPlayers) {
    this.refreshGamepadData();
    if (this.enabledGamepadIndices.size >= numberOfPlayers) {
      return;
    };
    gamepadDectection.detectGamepads();
  },
  refreshGamepadData : function() {
    // TODO convert return from navigator.getGamepads to a true array?
    this.gamepads = navigator.getGamepads();
    Object.keys(this.activeGamepads).forEach(key => {
      this.captureInputs(key);
    });
  },
  captureInputs : function(key) {
    let gamepad = this.getGamepad(key);
    if (gamepad.index > -1) {
      this.captureAxes(gamepad);
      this.captureButtons(gamepad);
    };
  },
  getGamepad : function(key) {
    return this.activeGamepads[key];
  },
  captureAxes : function(gamepad) {
    let inputValues = this.gamepads[gamepad.index].axes;
    this.saveAxes(inputValues, gamepad.axes);
  },
  isActive : function() {
    return this.enabledGamepadIndices.size > 0;
  },
  saveAxes : function(inputValues, axes) {
    Object.keys(axes).forEach(axis => {
      if (inputValues.length >= axes[axis].requiredArrayLength) {
        this.getAxis(inputValues, axes[axis]);
      };
    });
  },
  getAxis : function(inputValues, axis) {
    let indexX = axis.indices.x;
    let indexY = axis.indices.y;
    let threshold = gamepadConstants.stickValueThreshold;
    axis.values.x = Math.abs(inputValues[indexX]) > threshold ? inputValues[indexX] : 0;
    axis.values.y = Math.abs(inputValues[indexY]) > threshold ? inputValues[indexY] : 0;
  },
  captureButtons : function(gamepad) {
    gamepad.buttons = this.gamepads[gamepad.index].buttons.slice();
  },


  // entry point
  getObjectSpeed : function(baseSpeed, player) {
    let axes = this.activeGamepads[player.name].axes;
    return {
      x : axes.leftStick.values.x ? baseSpeed * Math.abs(axes.leftStick.values.x) : baseSpeed,
      y : axes.leftStick.values.y ? baseSpeed * Math.abs(axes.leftStick.values.y) : baseSpeed,
    };
  },
  validateDirection : function(direction, stick, player) {
    let directionIsActive = this.matchStickValuesToDirection(direction, stick, player);
    if (directionIsActive && this.boundaries && stick == 'leftStick') {
      this.alignLeftStickValuesToBoundaries(direction, player);
    };
    return directionIsActive;
  },
  matchStickValuesToDirection : function(direction, stick, player) {
    let stickValues = player.axes[stick].values;
    let compareObj = gamepadConstants.coordinateDirections[direction];
    let compareResult =
      compareObj.x == (stickValues.x >= 0 ? Math.ceil(stickValues.x) : Math.floor(stickValues.x))
        &&
      compareObj.y == (stickValues.y >= 0 ? Math.ceil(stickValues.y) : Math.floor(stickValues.y))
    ;
    return compareResult;
  },
  alignLeftStickValuesToBoundaries : function(direction, player) {
    let watchDirections = {
      'left' : ['upLeft', 'downLeft'],
      'right' : ['upRight', 'downRight'],
      'up' : ['upRight', 'upLeft'],
      'down' : ['downRight', 'downLeft'],
    };
    let axes = player.axes;
    axes.leftStick.values.y = watchDirections.up.includes(direction)    && !this.boundaries.belowTop    ? 0 : axes.leftStick.values.y;
    axes.leftStick.values.y = watchDirections.down.includes(direction)  && !this.boundaries.aboveBottom ? 0 : axes.leftStick.values.y;
    axes.leftStick.values.x = watchDirections.left.includes(direction)  && !this.boundaries.insideLeft  ? 0 : axes.leftStick.values.x;
    axes.leftStick.values.x = watchDirections.right.includes(direction) && !this.boundaries.insideRight ? 0 : axes.leftStick.values.x;
  },


  // entry point
  // only player 1 is allowed to pause
  pausePressed : function() {
    if (!this.isActive()) {
      return false;
    };
    let buttons = this.activeGamepads.player1.buttons;
    return buttons[gamepadConstants.pausedButtonIndex].pressed;
  },


  // entry point
  isFiring : function(player) {
    if (!player) {
      return false;
    };
    let activeGamepad = this.activeGamepads[player.name];
    let canFire = this.fireTriggered(activeGamepad);
    return this.isActive() && canFire;
  },
  fireTriggered : function(player) {
    let triggered = this.fireButtonPressed(player)
      || this.rightStickActive(player);
    return triggered;
  },
  fireButtonPressed : function(player) {
    let button = player.buttons.find(
      (button, index) => {return button.pressed && gamepadConstants.fireButtonIndices.includes(index)}
    );
    return button;
  },
  rightStickActive : function(player) {
    return Object.keys(gamepadConstants.coordinateDirections).find(dir =>
      this.validateDirection(dir, 'rightStick', player)
    );
  },


  // entry point
  // only player 1 is allowed to navigate menus
  menuSelect : function() {
    let button = this.activeGamepads.player1.buttons.find(
      (button, index) => {return button.pressed && gamepadConstants.menuEnterIndices.includes(index)}
    );
    return button;
  },


  // entry point
  // only player 1 is allowed to navigate menus
  detectMenuChange : function(checkDir, currentDir) {
    let sticks = ['dPad', 'leftStick'];
    sticks.forEach(stick => {
      currentDir = currentDir == "" && this.validateDirection(checkDir, stick, this.activeGamepads.player1)
        ? checkDir
        : currentDir
    });
    return currentDir;
  },
};

var gamepadDectection = {
  detectGamepads : function() {
    let gamepads = controls.gamepad.gamepads;
    for (let i = 0; i < gamepads.length; i++) {
      if (this.isUsableGamepad(gamepads[i], i)) {
        this.checkButtons(gamepads[i], i);
        this.checkAxes(gamepads[i], i);
      };
    };
  },
  isUsableGamepad : function(gamepad, index) {
    let notUsed = !Object.keys(controls.gamepad.activeGamepads).find(key =>
      controls.gamepad.activeGamepads[key].index == index
    );
    return gamepad && notUsed;
  },
  checkButtons : function(gamepad, index) {
    gamepad.buttons.forEach(button => {
      if (button.pressed) {
        console.log('button is pressed');
        controls.gamepad.enabledGamepadIndices.add(index);
        this.getUnassignedPlayer().index = index;
      };
    });
  },
  checkAxes : function(gamepad, index) {
    gamepad.axes.forEach(axis => {
      if (axis <= gamepadConstants.axesIndexLimit && Math.abs(axis) > 0.5) {
        console.log('axis is activated', axis);
        controls.gamepad.enabledGamepadIndices.add(index);
        let unassignedPlayer = this.getUnassignedPlayer();
        if (!unassignedPlayer) {
          console.log('no unassigned players left');
          return;
        };
        unassignedPlayer.index = index;
      };
    });
  },
  getUnassignedPlayer : function() {
    console.log(controls.gamepad.activeGamepads);
    let unassignedPlayerName = Object.keys(controls.gamepad.activeGamepads).find(key =>
      controls.gamepad.activeGamepads[key].index == -1
    );
    let unassignedPlayer = controls.gamepad.activeGamepads[unassignedPlayerName];
    console.log('unassigned playername:', unassignedPlayerName);
    return unassignedPlayer;
  },
};
