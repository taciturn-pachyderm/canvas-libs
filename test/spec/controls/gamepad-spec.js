describe('GAMEPAD SPEC: ', () => {
  let spec = 'GAMEPAD';
  beforeAll(function () {
    console.log('running ' + spec + ' SPEC');
  });
  afterAll(function () {
    console.log(spec + ' SPEC complete');
  });
  beforeEach(function() {
    testObj = Object.assign({}, gamepad);
  });

  it('isActive returns true when active', () => {
    testObj.enabled = true;
    testObj.index = 0;

    let expected = true;

    let actual = testObj.isActive();

    expect(actual).toEqual(expected);
  });

  it('pausePressed returns false when enabled is false', () => {
    testObj.enabled = false;
    let expected = false;

    let actual = testObj.pausePressed();

    expect(actual).toBe(expected);
  });
  it('pausePressed returns false when index is invalid', () => {
    testObj.enabled = true;
    testObj.index = -1;
    let expected = false;

    let actual = testObj.pausePressed();

    expect(actual).toBe(expected);
  });

  it('checkState does not match gamepads when navigator has empty gamepad array', () => {
    testObj.gamepads = undefined;
    testObj.index = -1;

    testObj.checkState();

    expect(testObj.index).toBe(-1);
  });
  it('checkState returns if enabledGamePads has reached max size', () => {
    spyOn(testObj, 'checkButtons');
    spyOn(testObj, 'checkAxes');
    testObj.enabledGamepadIndices.add({});
    testObj.enabledGamepadIndices.add({});

    testObj.checkState();

    expect(testObj.checkButtons).not.toHaveBeenCalled();
    expect(testObj.checkAxes).not.toHaveBeenCalled();
  });

  it('isFiring gamepad is not enabled, so returns false', () => {
    spyOn(testObj, 'isActive').and.returnValue(false);
    spyOn(testObj, 'fireButtonPressed').and.returnValue(true);
    spyOn(testObj, 'rightStickActive').and.returnValue(true);
    let expected = false;

    let actual = testObj.isFiring();

    expect(actual).toEqual(expected);
  });
  it('isFiring detects fire button press and returns true', () => {
    spyOn(testObj, 'isActive').and.returnValue(true);
    spyOn(testObj, 'fireButtonPressed').and.returnValue(true);
    spyOn(testObj, 'rightStickActive').and.returnValue(false);
    let expected = true;

    let actual = testObj.isFiring();

    expect(actual).toEqual(expected);
  });
  it('isFiring so checks rightStick when fire button not pressed,', () => {
    spyOn(testObj, 'isActive').and.returnValue(true);
    spyOn(testObj, 'fireButtonPressed').and.returnValue(false);
    spyOn(testObj, 'rightStickActive').and.returnValue(true);
    let expected = true;

    let actual = testObj.isFiring();

    expect(actual).toEqual(expected);
  });
 
  it('fireButtonPressed detects fire button press and returns true', () => {
    testObj.index = 0;
    testObj.gamepads = [{buttons : [{pressed : true}]}];
    let expected = true;

    let actual = testObj.fireButtonPressed().pressed;

    expect(actual).toEqual(expected);
  });

  it('rightStickActive returns true when direction is validated', () => {
    spyOn(testObj, 'validateDirection').and.returnValue(true);

    let actual = testObj.rightStickActive();

    expect(actual).toBeTruthy();
  });

});
