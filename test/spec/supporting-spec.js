describe('SUPPORTING SPEC: ', () => {
  let spec = 'SUPPORTING';
  beforeAll(function () {
    console.log('running ' + spec + ' SPEC');
  });
  afterAll(function () {
    console.log(spec + ' SPEC complete');
  });
  beforeEach(function() {
    testObj = Object.assign({}, supporting);
  });
  it('everyinterval should return true when n is a factor of frame number', () => {
    let frameNo = 10;
    expect(supporting.everyinterval(frameNo, 10)).toBeTruthy();
  });
  it('everyinterval should return false when n is a factor of frame number', () => {
    let frameNo = 3;
    expect(supporting.everyinterval(frameNo, 10)).not.toBeTruthy();
  });
  it('compare returns 0 if b.score equals a.score', () => {
    supporting.fieldToCompare = 'score';
    let a = {score : 1};
    let b = {score : 1};
    let expected = 0;

    let actual = supporting.compare(a, b);

    expect(actual).toBe(expected);
  });
  it('compare returns 1 if b.score is greater than a.score', () => {
    supporting.fieldToCompare = 'score';
    let a = {score : 1};
    let b = {score : 2};
    let expected = 1;

    let actual = supporting.compare(a, b);

    expect(actual).toBe(expected);
  });
  it('compare returns -1 if a.score is greater than b.score', () => {
    supporting.fieldToCompare = 'score';
    let a = {score : 2};
    let b = {score : 1};
    let expected = -1;

    let actual = supporting.compare(a, b);

    expect(actual).toBe(expected);
  });
  it('getFirstTruthy returns first truthy entry', () => {
    let expected = 'right';
    let notExpected = 'left';
    let entries = {'right' : true, 'left' : true};

    let actual = supporting.getFirstTruthy(entries);

    expect(actual).toEqual(expected);
  });

});
