describe('CONTROLS SPEC: ', () => {
  let spec = 'CONTROLS';
  beforeAll(function () {
    console.log('running ' + spec + ' SPEC');
  });
  afterAll(function () {
    console.log(spec + ' SPEC complete');
  });
  beforeEach(function() {
    testObj = Object.assign({}, controls);
  });
  function defaultPlayerBoundaries() {
    testObj.gamepad.boundaries = {
      belowTop: true,
      aboveBottom: true,
      insideLeft: true,
      insideRight : true,
    };
  };
  it('getPositionModifiers returns expected coordinate values', () => {
    spyOn(testObj.gamepad, 'getObjectSpeed');
    spyOn(testObj, 'buildSpeedMods').and.returnValue(testObj.gamepad.coordinateDirections);
    let expected = 'right';
    spyOn(testObj, 'getActiveDirection').and.returnValue([expected]);
    let expectedValue = {x : 1, y : 0};

    let actual = testObj.getPositionModifiers(1, {expected: expectedValue});

    expect(actual).toEqual(expectedValue);
  });

  it('getObjectSpeed returns multiplied leftStick values', () => {
    let testValues = {x : 1, y : 1};
    testObj.gamepad.axes.leftStick.values = testValues;
    let expected = Object.assign({}, testValues);
    let baseSpeed = 2;
    expected.x = expected.x * baseSpeed;
    expected.y = expected.y * baseSpeed;

    let actual = testObj.gamepad.getObjectSpeed(baseSpeed);

    expect(actual).toEqual(expected);
  });
  it('getObjectSpeed returns baseSpeed values', () => {
    let testValues = {x : 0, y : 0};
    testObj.gamepad.axes.leftStick.values = testValues;
    let expected = Object.assign({}, testValues);
    let baseSpeed = 2;
    expected.x = baseSpeed;
    expected.y = baseSpeed;

    let actual = testObj.gamepad.getObjectSpeed(baseSpeed);

    expect(actual).toEqual(expected);
  });

  it('buildSpeedMods determines values for all available directions', () => {
    let testValues = {x : 1, y : 1};
    let expected = {
      upRight: {x : 1, y : -1},
      upLeft: {x : -1, y : -1},
      downRight: {x : 1, y : 1},
      downLeft: {x : -1, y : 1},
      right: {x : 1},
      down: {y : 1},
      left: {x : -1},
      up: {y : -1},
    };
    let actual = testObj.buildSpeedMods(testValues);

    expect(actual).toEqual(expected);
  });

  it('getActiveDirection returns the first active direction in movementCodes keys', () => {
    spyOn(testObj, 'getValidDirections');
    spyOn(supporting, 'getFirstTruthy');

    testObj.getActiveDirection();

    expect(testObj.getValidDirections).toHaveBeenCalled();
    expect(supporting.getFirstTruthy).toHaveBeenCalled();
  });

  it('getValidDirections returns object with all movementCodes keys, setting valid directions to true', () => {
    spyOn(testObj, 'checkDirection').and.returnValue(true);
    let expected = {'right' : true, 'left' : true};

    let actual = testObj.getValidDirections(expected);

    expect(actual['left']).toEqual(expected['left']);
    expect(actual['right']).toEqual(expected['right']);
  });

  it('checkDirection returns true when isWholeArrayPresent is true', () => {
    spyOn(testObj, 'isWholeArrayPresent').and.returnValue(true);
    spyOn(testObj.gamepad, 'matchStickValuesToDirection').and.returnValue(false);

    let result = testObj.checkDirection('left');

    expect(result).toBe(true);
    expect(testObj.isWholeArrayPresent).toHaveBeenCalled();
    expect(testObj.gamepad.matchStickValuesToDirection).not.toHaveBeenCalled();
  });
  it('checkDirection returns true when matchStickValuesToDirection is true', () => {
    spyOn(testObj, 'isWholeArrayPresent').and.returnValue(false);
    spyOn(testObj.gamepad, 'matchStickValuesToDirection').and.returnValue(true);

    let result = testObj.checkDirection('left');

    expect(result).toBe(true);
    expect(testObj.isWholeArrayPresent).toHaveBeenCalled();
    expect(testObj.gamepad.matchStickValuesToDirection).toHaveBeenCalled();
  });

  it('isWholeArrayPresent returns false when value is missing', () => {
    testValues = ['0', '1'];
    testObj.keysDown = {'0' : true};

    let result = testObj.isWholeArrayPresent(testValues, testObj.keysDown);

    expect(result).toBe(false);
  });

  it('isWholeArrayPresent returns true when all values are present', () => {
    testValues = ['0', '1'];
    testObj.keysDown = {'0' : true, '1' : true, '2' : true};

    let result = testObj.isWholeArrayPresent(testValues, testObj.keysDown);

    expect(result).toBe(true);
  });

  it('validateDirection does not call alignLeftStick...', () => {
    spyOn(testObj.gamepad, 'matchStickValuesToDirection').and.returnValue(false);
    spyOn(testObj.gamepad, 'alignLeftStickValuesToBoundaries');

    let result = testObj.gamepad.validateDirection();

    expect(result).toBe(false);
    expect(testObj.gamepad.matchStickValuesToDirection).toHaveBeenCalled();
    expect(testObj.gamepad.alignLeftStickValuesToBoundaries).not.toHaveBeenCalled();
  });
  it('validateDirection returns true if matchStickValuesToDirection is true', () => {
    spyOn(testObj.gamepad, 'matchStickValuesToDirection').and.returnValue(true);
    spyOn(testObj.gamepad, 'alignLeftStickValuesToBoundaries');

    let result = testObj.gamepad.validateDirection('left', 'rightStick');

    expect(result).toBe(true);
    expect(testObj.gamepad.matchStickValuesToDirection).toHaveBeenCalled();
    expect(testObj.gamepad.alignLeftStickValuesToBoundaries).not.toHaveBeenCalled();
  });
  it('validateDirection calls alignLeftStick...', () => {
    spyOn(testObj.gamepad, 'matchStickValuesToDirection').and.returnValue(true);
    spyOn(testObj.gamepad, 'alignLeftStickValuesToBoundaries');
    testObj.gamepad.boundaries = {};

    let result = testObj.gamepad.validateDirection('left', 'leftStick');

    expect(result).toBe(true);
    expect(testObj.gamepad.matchStickValuesToDirection).toHaveBeenCalled();
    expect(testObj.gamepad.alignLeftStickValuesToBoundaries).toHaveBeenCalled();
  });

  it('matchStickValuesToDirection returns true when both stick values match sign of coordinateDirections', () => {
    let compareObj = {x : 0, y : -1};
    let direction = 'up';
    let stick = 'leftStick';
    testObj.gamepad.coordinateDirections.up = compareObj;
    testObj.gamepad.axes.leftStick.values = Object.assign({}, compareObj);
    let expected = true;

    let actual = testObj.gamepad.matchStickValuesToDirection(direction, stick);

    expect(actual).toEqual(expected);
  });
  it('matchStickValuesToDirection returns false when both stick values do not match sign of coordinateDirections', () => {
    let compareObj = {x : 0, y : -1};
    let direction = 'up';
    let stick = 'leftStick';
    testObj.gamepad.coordinateDirections.up = compareObj;
    testObj.gamepad.axes.leftStick.values = Object.assign({}, compareObj);
    testObj.gamepad.axes.leftStick.values.x = -1;
    let expected = false;

    let actual = testObj.gamepad.matchStickValuesToDirection(direction, stick);

    expect(actual).toEqual(expected);
  });
  it('matchStickValuesToDirection returns true when both stick values match sign of coordinateDirections', () => {
    let compareObj = {x : 0, y : 1};
    let direction = 'up';
    let stick = 'leftStick';
    testObj.gamepad.coordinateDirections.up = compareObj;
    testObj.gamepad.axes.leftStick.values = Object.assign({}, compareObj);
    let expected = true;

    let actual = testObj.gamepad.matchStickValuesToDirection(direction, stick);

    expect(actual).toEqual(expected);
  });

  it('alignLeftStickValuesToBoundaries sets y to 0 when outside top bound', () => {
    defaultPlayerBoundaries();
    testObj.gamepad.boundaries.belowTop = false;
    testObj.gamepad.axes.leftStick.values = {x : 1, y : -1};

    testObj.gamepad.alignLeftStickValuesToBoundaries('upRight');

    expect(testObj.gamepad.axes.leftStick.values).toEqual({x : 1, y : 0});
  });
  it('alignLeftStickValuesToBoundaries sets y to 0 when outside bottom bound', () => {
    defaultPlayerBoundaries();
    testObj.gamepad.boundaries.aboveBottom = false;
    testObj.gamepad.axes.leftStick.values = {x : 1, y : 1};

    testObj.gamepad.alignLeftStickValuesToBoundaries('downLeft');

    expect(testObj.gamepad.axes.leftStick.values).toEqual({x : 1, y : 0});
  });
  it('alignLeftStickValuesToBoundaries sets x to 0 when outside left bound', () => {
    defaultPlayerBoundaries();
    testObj.gamepad.boundaries.insideLeft = false;
    testObj.gamepad.axes.leftStick.values = {x : -1, y : 1};

    testObj.gamepad.alignLeftStickValuesToBoundaries('downLeft');

    expect(testObj.gamepad.axes.leftStick.values).toEqual({x : 0, y : 1});
  });
  it('alignLeftStickValuesToBoundaries sets x to 0 when outside right bound', () => {
    defaultPlayerBoundaries();
    testObj.gamepad.boundaries.insideRight = false;
    testObj.gamepad.axes.leftStick.values = {x : 1, y : 1};

    testObj.gamepad.alignLeftStickValuesToBoundaries('downRight');

    expect(testObj.gamepad.axes.leftStick.values).toEqual({x : 0, y : 1});
  });
  it('alignLeftStickValuesToBoundaries leaves values unchanged when within bounds', () => {
    defaultPlayerBoundaries();
    testObj.gamepad.axes.leftStick.values = {x : 1, y : 1};

    testObj.gamepad.alignLeftStickValuesToBoundaries('downRight');

    expect(testObj.gamepad.axes.leftStick.values).toEqual({x : 1, y : 1});
  });

  it('captureGamepadAxes gets axes for all keys in testObj.axes', () => {
    testObj.gamepad.enabled = true;
    testObj.gamepad.index = 0;
    testObj.gamepad.gamepads = [{axes : [0, 0, 0, 0, 0, 0]}];
    spyOn(testObj.gamepad, 'getAxis');

    testObj.gamepad.captureAxes();

    expect(testObj.gamepad.getAxis).toHaveBeenCalledTimes(3);
  });
  it('captureGamepadAxes does not get dPad axes if gamePads axes array length is not long enough', () => {
    testObj.gamepad.enabled = true;
    testObj.gamepad.index = 0;
    testObj.gamepad.gamepads = [{axes : [0, 0, 0, 0]}];
    spyOn(testObj.gamepad, 'getAxis');

    testObj.gamepad.captureAxes();

    expect(testObj.gamepad.getAxis).toHaveBeenCalledTimes(2);
  });
  it('captureAxes returns without getting axes if gamepad.enabled is false', () => {
    testObj.gamepad.enabled = false;
    testObj.gamepad.index = 0;
    spyOn(testObj.gamepad, 'getAxis');

    testObj.gamepad.captureAxes();

    expect(testObj.gamepad.getAxis).not.toHaveBeenCalled();
  });
  it('captureGamepadAxes returns without getting axes if gamepad.index is invalid', () => {
    testObj.gamepad.enabled = true;
    testObj.gamepad.index = -1;
    spyOn(testObj.gamepad, 'getAxis');

    testObj.gamepad.captureAxes();

    expect(testObj.gamepad.getAxis).not.toHaveBeenCalled();
  });

  it('getAxis returns value of 0 if below threshold', () => {
    testObj.stickValueThreshold = 0.25;
    let inputValues = [0.2, 0.2];

    testObj.gamepad.getAxis('leftStick', inputValues);

    expect(testObj.gamepad.axes.leftStick.values).toEqual({x : 0, y : 0});
  });
  it('getAxis returns axis value if above threshold', () => {
    testObj.gamepad.stickValueThreshold = 0.25;
    let inputValues = [0.7, 0.5];

    testObj.gamepad.getAxis('leftStick', inputValues);

    expect(testObj.gamepad.axes.leftStick.values).toEqual({x : 0.7, y : 0.5});
  });

  it('gamepadPausePressed returns true paused button is pressed', () => {
    testObj.gamepad.enabled = true;
    testObj.gamepad.index = 0;
    testObj.gamepad.pausedButtonIndex = 1;
    testObj.gamepad.gamepads = [{buttons : [{pressed : false}, {pressed : true}]}];
    let expected = true;

    let actual = testObj.gamepad.pausePressed();

    expect(actual).toBe(expected);
  });

  it('keyBoardFlowControlButtonPressed returns true if matching keyId is pressed', () => {
    testObj.keyboard.flowControlKeyCodes = [3];
    spyOn(testObj.keyboard, 'getPressedKeys').and.returnValue(["3"]);
    let expected = true;

    let actual = testObj.keyboard.flowControlButtonPressed();

    expect(testObj.keyboard.getPressedKeys).toHaveBeenCalled();
    expect(actual).toBe(expected);
  });
  it('keyBoardFlowControlButtonPressed returns false if matching keyId is not pressed', () => {
    testObj.keyboard.flowControlKeyCodes = [4];
    spyOn(testObj.keyboard, 'getPressedKeys').and.returnValue(["3"]);
    let expected = false;

    let actual = testObj.keyboard.flowControlButtonPressed();

    expect(testObj.keyboard.getPressedKeys).toHaveBeenCalled();
    expect(actual).toBe(expected);
  });

  it('getPressedKeys returns all keysDown true keys', () => {
    testObj.keyboard.keysDown = {3 : true, 4 : false};
    let expected = ["3"];

    let actual = testObj.keyboard.getPressedKeys();

    expect(actual).toEqual(expected);

    testObj.keyboard.keysDown = {3 : false, 4 : true};
    expected = ["4"];

    actual = testObj.keyboard.getPressedKeys();

    expect(actual).toEqual(expected);
  });

  it('isFiring does not call keyboard.isFiring if gamepad.isFiring is true', () => {
    spyOn(gamepad, 'isFiring').and.returnValue(true);
    spyOn(keyboard, 'isFiring').and.returnValue(true);
    let expected = true;

    let actual = testObj.isFiring();

    expect(actual).toBe(expected);
    expect(gamepad.isFiring).toHaveBeenCalled();
    expect(keyboard.isFiring).not.toHaveBeenCalled();
  });
  it('isFiring calls keyboard.isFiring if gamepad.isFiring is false', () => {
    spyOn(gamepad, 'isFiring').and.returnValue(false);
    spyOn(keyboard, 'isFiring').and.returnValue(true);
    let expected = true;

    let actual = testObj.isFiring();

    expect(actual).toBe(expected);
    expect(gamepad.isFiring).toHaveBeenCalled();
    expect(keyboard.isFiring).toHaveBeenCalled();
  });
  it('isFiring gets false from both keyboard.isFiring and gamepad.isFiring and returns false', () => {
    spyOn(gamepad, 'isFiring').and.returnValue(false);
    spyOn(keyboard, 'isFiring').and.returnValue(false);
    let expected = false;

    let actual = testObj.isFiring();

    expect(actual).toBe(expected);
    expect(gamepad.isFiring).toHaveBeenCalled();
    expect(keyboard.isFiring).toHaveBeenCalled();
  });

  it('pausedIsPressed does not call keyboard.flowControlButtonPressed if gamepad.pausePressed is true', () => {
    spyOn(gamepad, 'pausePressed').and.returnValue(true);
    spyOn(keyboard, 'flowControlButtonPressed').and.returnValue(true);
    let expected = true;

    let actual = testObj.pausedIsPressed();

    expect(actual).toBe(expected);
    expect(gamepad.pausePressed).toHaveBeenCalled();
    expect(keyboard.flowControlButtonPressed).not.toHaveBeenCalled();
  });
  it('pausedIsPressed calls keyboard.flowControlButtonPressed if gamepad.pausePressed is false', () => {
    spyOn(gamepad, 'pausePressed').and.returnValue(false);
    spyOn(keyboard, 'flowControlButtonPressed').and.returnValue(true);
    let expected = true;

    let actual = testObj.pausedIsPressed();

    expect(actual).toBe(expected);
    expect(gamepad.pausePressed).toHaveBeenCalled();
    expect(keyboard.flowControlButtonPressed).toHaveBeenCalled();
  });
  it('pausedIsPressed gets false from both keyboard.flowControlButtonPressed and gamepad.pausePressed and returns false', () => {
    spyOn(gamepad, 'pausePressed').and.returnValue(false);
    spyOn(keyboard, 'flowControlButtonPressed').and.returnValue(false);
    let expected = false;

    let actual = testObj.pausedIsPressed();

    expect(actual).toBe(expected);
    expect(gamepad.pausePressed).toHaveBeenCalled();
    expect(keyboard.flowControlButtonPressed).toHaveBeenCalled();
  });

  it('getDirection returns menu movement direction', () => {
    spyOn(testObj, 'checkMenuDirection').and.returnValue('');
    let expected = "";

    let actual = testObj.getDirection();

    expect(actual).toEqual(expected);
    expect(testObj.checkMenuDirection).toHaveBeenCalledTimes(4);
  });

  it('checkMenuDirection delegates to keyboard and gamepad.checkMenuDirection', () => {
    spyOn(controls.keyboard, 'checkMenuDirection');
    spyOn(controls.gamepad, 'checkMenuDirection');

    testObj.checkMenuDirection('', '');

    expect(controls.keyboard.checkMenuDirection).toHaveBeenCalled();
    expect(controls.gamepad.checkMenuDirection).toHaveBeenCalled();
  });

  xit('keyboard.checkMenuDirection returns menu movement direction', () => {
    let testValues = {up : 0, down : 1};
    spyOn(controls.keyboard, 'getPressedKeys').and.returnValue(["1"]);
    controls.keyboard.movementCodes.up = [testValues.up];
    controls.keyboard.movementCodes.down = [testValues.down];
    let expected = 'down';
    let checkDir = 'up';

    let actual = testObj.keyboard.checkMenuDirection(checkDir, expected);

    expect(controls.keyboard.getPressedKeys).toHaveBeenCalled();
    expect(actual).toEqual(expected);
  });

  it('gamepad.checkMenuDirection returns unmodified menu movement when gamepadDirection does not validate', () => {
    spyOn(testObj.gamepad, 'validateDirection').and.returnValue(false);
    let checkDir = 'aThing';
    let currentDir = 'anotherThing';
    let expected = currentDir;

    let actual = testObj.gamepad.checkMenuDirection(checkDir, currentDir);

    expect(actual).toEqual(expected);
  });
  it('gamepad.checkMenuDirection returns modified menu movement when gamepadDirection validates', () => {
    spyOn(testObj.gamepad, 'validateDirection').and.returnValue(true);
    let checkDir = 'aThing';
    let currentDir = '';
    let expected = checkDir;

    let actual = testObj.gamepad.checkMenuDirection(checkDir, currentDir);

    expect(actual).toEqual(expected);
  });
});
