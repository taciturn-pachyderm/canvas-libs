module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      'app/scripts/controls/*.js',
      'app/scripts/*.js',
      'test/spec/*.js',
      'test/spec/*/*.js',
    ],
    browsers: ['Chrome'],
    plugins : ['karma-jasmine', 'karma-phantomjs-launcher', 'karma-coverage', 'karma-chrome-launcher'],
    singleRun: true,
    reporters: ['progress', 'coverage'],
    preprocessors: {
      'app/scripts/*.js': ['coverage'],
      'app/scripts/*/*.js': ['coverage'],
    },
  });
};
